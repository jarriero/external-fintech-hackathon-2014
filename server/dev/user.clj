(ns user
  "Namespace to support hacking at the REPL."
  (:require [clj-hack-the-bank.system :as app]
            [clojure.tools.namespace.repl :refer (refresh refresh-all)]
            [clojure.tools.namespace.move :refer :all]
            [clojure.repl :refer :all]
            [clojure.pprint :refer :all]
            [spyscope.core]
            [cemerick.pomegranate :refer [add-dependencies]]
            [clojure.java.io :as cjio]
            [clojure.string :as str]
            [clojure.java.classpath :as cjc]
            [criterium.core :as crit]
            [com.stuartsierra.component :as component]))

(def dev-port 7601)

(def system  nil)

(defn init []
  (alter-var-root #'system
    (constantly (app/create-system {:port dev-port :datastore-file-name "jam-jar-datastore.edn"}))))

(defn start []
  (alter-var-root #'system component/start))

(defn stop []
  (alter-var-root #'system
    (fn [s] (when s (component/stop s)))))

(defn go []
  (init)
  (start))

(defn reset []
  (stop)
  (refresh :after 'user/go))
