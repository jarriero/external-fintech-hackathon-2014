(defproject clj-hack-the-bank "0.1.0-SNAPSHOT"
  :description "Erudine teams '2014 hack the bank' hackation project"
  :url "http://erudine.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  ;;:main clj-hack-the-bank.system
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [com.stuartsierra/component "0.2.1"]
                 [compojure "1.1.5"]
                 [ring/ring-core "1.1.8"]
                 [ring/ring-jetty-adapter "1.1.8"]
                 [ring/ring-json "0.2.0"]
                 [org.clojure/data.json "0.2.4"]
                 [enlive "1.1.1"]                 
                 [org.clojure/test.check "0.5.7"]
                 [clj-http "0.9.1"]
                 [org.clojure/tools.trace "0.7.8"]
                 [org.clojure/core.async "0.1.267.0-0d7780-alpha"]
                 [log4j/log4j "1.2.17"]
                 [org.clojure/tools.logging "0.2.6"]
                 [ring-mock "0.1.5"]
                 [org.clojure/tools.namespace "0.2.3"]
                 [com.paypal.sdk/adaptivepaymentssdk "2.5.106"]
                 [com.paypal.sdk/merchantsdk "2.6.109"]]
  :java-source-paths ["src/java"]
  
  :profiles
  {:dev {:source-paths ["dev"]
         :dependencies [[org.clojure/math.combinatorics "0.0.4"]
                        [ring-mock "0.1.5"]
                        [org.clojure/tools.namespace "0.2.3"]
                        [com.cemerick/pomegranate "0.2.0"]
                        ;; [slamhound "1.3.3"]
                        [spyscope "0.1.3"]
                        [criterium "0.4.1"]
                        [org.clojure/java.classpath "0.2.0"]]}})
