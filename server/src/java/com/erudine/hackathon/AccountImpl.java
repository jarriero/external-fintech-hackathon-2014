package com.erudine.hackathon;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.PayRequest;
import com.paypal.svcs.types.ap.Receiver;
import com.paypal.svcs.types.ap.ReceiverList;
import com.paypal.svcs.types.common.RequestEnvelope;
import urn.ebay.api.PayPalAPI.*;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionSearchResultType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public class AccountImpl implements Account {
    private String userAccountDetails;
    private AdaptivePaymentsService userAccountPaymentsService;
    private PayPalAPIInterfaceServiceService userAccountTransactionDetailsService;

    @Override
    public void setAccount(Map<String, String> accountDetails) {
        //TODO Sanity checking/defensive copying.
    	accountDetails.put("mode", "sandbox");
        userAccountPaymentsService = new AdaptivePaymentsService(accountDetails);
        userAccountTransactionDetailsService = new PayPalAPIInterfaceServiceService(accountDetails);
    }

    @Override
    public void makePayment(Account payee, int pence) {
        try{
            userAccountPaymentsService.pay(createPayRequest(payee.getUserAccountDetails(), pence));
        }
        catch(Exception e){
            //TODO Deal with exceptions properly.
            e.printStackTrace();
        }
    }

    @Override
    public int getBalanceInPence() {
        GetBalanceReq balanceRequest = new GetBalanceReq();
        GetBalanceRequestType balanceRequestType = new GetBalanceRequestType();
        balanceRequestType.setReturnAllCurrencies("0");
        balanceRequest.setGetBalanceRequest(balanceRequestType);
        try {
            GetBalanceResponseType balance = userAccountTransactionDetailsService.getBalance(balanceRequest);
            BasicAmountType amount = balance.getBalance();
            Double total = Double.parseDouble(amount.getValue());
            return (int) (total * 100);
        }
            catch(Exception e){
                //TODO Deal with exceptions properly.
                e.printStackTrace();
                return 0;
            }
    }

    @Override
    public List<Transaction> getTransactions(SearchCriteria searchCriteria) {
        List<Transaction> matchingTransactions = new ArrayList<Transaction>();
        TransactionSearchReq searchRequest = createTransactionSearchReq(searchCriteria);
        TransactionSearchResponseType searchResults = searchForTransactions(searchRequest);
        return createTransactionList(searchResults);
    }

    private List<Transaction> createTransactionList(TransactionSearchResponseType searchResults){
        List<Transaction> results = new ArrayList<Transaction>();
        for(PaymentTransactionSearchResultType result : searchResults.getPaymentTransactions()){
            results.add(createTransaction(result));
        }
        return results;
    }

    private Transaction createTransaction(PaymentTransactionSearchResultType transactionInput){
        return new TransactionImpl(transactionInput, userAccountDetails);
    }

    private TransactionSearchResponseType searchForTransactions(TransactionSearchReq searchRequest){
        try{
            return userAccountTransactionDetailsService.transactionSearch(searchRequest);
        }
        catch(Exception e){
            //TODO Deal with exceptions properly.
            e.printStackTrace();
            return null;
        }
    }

    private TransactionSearchReq createTransactionSearchReq(SearchCriteria searchCriteria){
        TransactionSearchReq searchRequest = new TransactionSearchReq();
        TransactionSearchRequestType requestConfiguration = configureSearchCriteria(searchCriteria);
        searchRequest.setTransactionSearchRequest(requestConfiguration);
        return searchRequest;
    }

    private TransactionSearchRequestType configureSearchCriteria(SearchCriteria searchCriteria){
        TransactionSearchRequestType searchRequestType = new TransactionSearchRequestType();
        searchRequestType.setStartDate(searchCriteria.getStartDate());
        for(Criterion criterion : searchCriteria.getCriteria()){
            searchRequestType = configureCriterion(searchRequestType, criterion);
        }
        return searchRequestType;
    }

    private TransactionSearchRequestType configureCriterion(TransactionSearchRequestType searchRequestType, Criterion criterion){
        if(criterion.getParameter().equals("Payer")){
            searchRequestType.setPayer(criterion.getValue());
        }
        else if(criterion.getParameter().equals("Amount")){
            searchRequestType.setAmount(new BasicAmountType(CurrencyCodeType.GBP, criterion.getValue()));
        }
        else if(criterion.getParameter().equals("EndDate")){
            searchRequestType.setEndDate(criterion.getValue());
        }
        else if(criterion.getParameter().equals("Receiver")){
            searchRequestType.setReceiver(criterion.getValue());
        }
        else if(criterion.getParameter().equals("TransactionID")){
            searchRequestType.setTransactionID(criterion.getValue());
        }
        return searchRequestType;
    }

    @Override
    public String getUserAccountDetails() {
        return userAccountDetails;
    }

    @Override
    public void setUserAccountDetails(String userAccountDetails) {
        this.userAccountDetails = userAccountDetails;
    }

    private RequestEnvelope createRequestEnvelope(){
        RequestEnvelope envelope = new RequestEnvelope();
        envelope.setErrorLanguage("en_US");
        return envelope;
    }

    private Receiver createReceiver(String receiverAccountDetails, int paymentInPence){
        Receiver receiver = new Receiver();
        receiver.setAmount(paymentInPence * 0.01);
        receiver.setEmail(receiverAccountDetails);
        return receiver;
    }

    private PayRequest createPayRequest(String receiverAccountDetails, int paymentInPence){
        PayRequest payRequest = new PayRequest();
        payRequest.setActionType("PAY");
        payRequest.setCurrencyCode("GBP");
        payRequest.setCancelUrl("https://localhost");
        payRequest.setReturnUrl("https://localhost");
        payRequest.setSenderEmail(userAccountDetails);

        List<Receiver> receivers = new ArrayList<Receiver>();
        receivers.add(createReceiver(receiverAccountDetails, paymentInPence));
        payRequest.setReceiverList(new ReceiverList(receivers));
        payRequest.setRequestEnvelope(createRequestEnvelope());
        return payRequest;
    }
}
