package com.erudine.hackathon;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public interface Transaction {

    /**
     * For the purposes of this Hackathon, this is the payee's Paypal email address.
     * @return the Paypal email address of the payee.
     */
    public String getPayeeDetails();

    /**
     * For the purposes of this Hackathon, this is the payee's Paypal email address.
     * @return the Paypal email address of the payer.
     */
    public String getPayerDetails();

    public int getAmountPaidInPence();

    public String getTimestamp();

    public String getTransactionID();

}
