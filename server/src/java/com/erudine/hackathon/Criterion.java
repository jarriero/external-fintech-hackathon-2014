package com.erudine.hackathon;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public class Criterion {

    private String parameter;
    private String value;

    public Criterion(String parameter, String value){
        //TODO Sanity checking on input.
        this.parameter = parameter;
        this.value = value;
    }

    public String getParameter() {
        return parameter;
    }

    public String getValue() {
        return value;
    }
}
