package com.erudine.hackathon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public class SearchCriteria {

    private String startDate;
    private List<Criterion> criteria;

    public SearchCriteria(String startDate){
        //TODO Sanity checking on date.
        this.startDate = startDate;
        criteria = new ArrayList<Criterion>();
    }

    /**
     * Valid searchParameters are Payer, Amount, EndDate, Receiver, and TransactionID. Amount value should be in pounds.
     * EndDate should be in UTC/GMT format. Payer and Receiver values should be the Paypal email addresses.
     */
    public void addSearchCriterion(String searchParameter, String searchParameterValue){
        //TODO Sanity checking on input.
        if(searchParameter.equals("Amount")){
            Double amountInPounds = Double.parseDouble(searchParameterValue);
            int amountInPence = (int)(amountInPounds * 100);
            searchParameterValue = "" + amountInPence;
        }
        criteria.add(new Criterion(searchParameter, searchParameterValue));
    }

    public String getStartDate(){
        return startDate;
    }

    public List<Criterion> getCriteria(){
        return criteria;
    }

}
