package com.erudine.hackathon;

import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public interface Account {

    /**
     * Essential account parameters to define an account. For the Hackathon, this would be a Paypal UserName, Password,
     * Signature, and AppId.
     * @param accountDetails
     */
    public void setAccount(Map<String, String> accountDetails);

    public void makePayment(Account payee, int pence);

    public int getBalanceInPence();

    public List<Transaction> getTransactions(SearchCriteria searchCriteria);

    /**
     * For the purposes of this Hackathon, this will be the Paypal email address.
     * @return the email address associated with this Paypal account.
     */
    public String getUserAccountDetails();

    /**
     * For the purposes of this Hackathon, this will be the Paypal email address.
     * @param userAccountDetails the email address associated with this Paypal account.
     */
    public void setUserAccountDetails(String userAccountDetails);

}
