package com.erudine.hackathon;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.PayRequest;
import com.paypal.svcs.types.ap.PayResponse;
import com.paypal.svcs.types.ap.Receiver;
import com.paypal.svcs.types.ap.ReceiverList;
import com.paypal.svcs.types.common.DetailLevelCode;
import com.paypal.svcs.types.common.RequestEnvelope;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.TransactionSearchReq;
import urn.ebay.api.PayPalAPI.TransactionSearchRequestType;
import urn.ebay.api.PayPalAPI.TransactionSearchResponseType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionSearchResultType;

import java.util.*;

/**
 * Created by Benjamin Woolford-Lim on 19/03/2014.
 */
public class PaypalTest {

    public static void main(String[] args) throws Exception{

//        new com.erudine.hackathon.PaypalTest().testPayment();
        new PaypalTest().testTransactionSearch();
//        new com.erudine.hackathon.PaypalTest().testBalanceFormat();
    }

    public void testPayment() throws Exception {

        RequestEnvelope envelope = new RequestEnvelope();
        envelope.setErrorLanguage("en_US");
        envelope.setDetailLevel(DetailLevelCode.RETURNALL);

        List<Receiver> recipients = new ArrayList<Receiver>();
        Receiver receiver = new Receiver();
        receiver.setAmount(2.0);
        receiver.setEmail("ringfence@provider.com");
        recipients.add(receiver);
        ReceiverList receiverList = new ReceiverList(recipients);

        PayRequest payRequest = new PayRequest();
        payRequest.setActionType("PAY");
        payRequest.setCurrencyCode("GBP");
        payRequest.setCancelUrl("https://localhost");
        payRequest.setReturnUrl("https://localhost");
        payRequest.setSenderEmail("payments@dwp.co.uk");

        payRequest.setReceiverList(receiverList);
        payRequest.setRequestEnvelope(envelope);

        Map<String, String> customConfigurationMap = new HashMap<String, String>();
        customConfigurationMap.put("mode", "sandbox"); // Load the map with all mandatory parameters
        customConfigurationMap.put("acct1.UserName", "payments_api1.dwp.co.uk");
        customConfigurationMap.put("acct1.Password", "1395311279");
        customConfigurationMap.put("acct1.Signature", "AFcWxV21C7fd0v3bYYYRCpSSRl31Ar-XaHg97wQl4NiJ-gcS4pdGzsES");
        customConfigurationMap.put("acct1.AppId", "APP-80W284485P519543T");

        AdaptivePaymentsService adaptivePaymentsService = new AdaptivePaymentsService(customConfigurationMap);
        PayResponse payResponse = adaptivePaymentsService.pay(payRequest);
    }

    public void testTransactionSearch() throws Exception{

        Map<String, String> customConfigurationMap = new HashMap<String, String>();
        customConfigurationMap.put("mode", "sandbox"); // Load the map with all mandatory parameters
        customConfigurationMap.put("acct1.UserName", "ringfence_api1.provider.com");
        customConfigurationMap.put("acct1.Password", "1395323922");
        customConfigurationMap.put("acct1.Signature", "AOS-1KuY5hgl0sjFfvV3llwvoZt6AliKA827wKIYHgOT9QZPP9oEy1PQ");
        customConfigurationMap.put("acct1.AppId", "APP-80W284485P519543T");
        PayPalAPIInterfaceServiceService merchantService = new PayPalAPIInterfaceServiceService(customConfigurationMap);

        TransactionSearchReq txnreq = new TransactionSearchReq();
        TransactionSearchRequestType requestType = new TransactionSearchRequestType();

        requestType.setStartDate("2014-03-19T05:38:48Z");
        requestType.setVersion("95.0");
        requestType.setPayer("payments@dwp.co.uk");
        //requestType.setTransactionID("9D572407MD3155605");
        txnreq.setTransactionSearchRequest(requestType);


        TransactionSearchResponseType txnresponse = merchantService.transactionSearch(txnreq);

        for(PaymentTransactionSearchResultType result : txnresponse.getPaymentTransactions()){
            System.out.println("Payer Display Name: " + result.getPayerDisplayName());
            System.out.println("Payer : " + result.getPayer());
            System.out.println("Type: " + result.getType());
        }
    }

    public void testBalanceFormat(){
        Account dwp = new AccountImpl();
        Map<String, String> customConfigurationMap = new HashMap<String, String>();
        customConfigurationMap.put("mode", "sandbox"); // Load the map with all mandatory parameters
        customConfigurationMap.put("acct1.UserName", "payments_api1.dwp.co.uk");
        customConfigurationMap.put("acct1.Password", "1395311279");
        customConfigurationMap.put("acct1.Signature", "AFcWxV21C7fd0v3bYYYRCpSSRl31Ar-XaHg97wQl4NiJ-gcS4pdGzsES");
        customConfigurationMap.put("acct1.AppId", "APP-80W284485P519543T");
        dwp.setAccount(customConfigurationMap);
        dwp.setUserAccountDetails("payments@dwp.co.uk");
        System.out.println("Balance: " + dwp.getBalanceInPence());
    }
}
