package com.erudine.hackathon;

import com.paypal.core.LoggingManager;
import com.paypal.ipn.IPNMessage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Benjamin Woolford-Lim on 20/03/2014.
 */
public class IPNListener extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // For a full list of configuration parameters refer in wiki page.
        // (https://github.com/paypal/sdk-core-java/wiki/SDK-Configuration-Parameters)
        Map<String, String> configurationMap = new HashMap<String, String>();
        configurationMap.put("mode", "sandbox"); // Load the map with all mandatory parameters
        IPNMessage ipnlistener = new IPNMessage(request,configurationMap);
        boolean isIpnVerified = ipnlistener.validate();
        String transactionType = ipnlistener.getTransactionType();
        Map<String,String> map = ipnlistener.getIpnMap();

        LoggingManager.info(IPNListener.class, "******* IPN (name:value) pair : " + map + "  " +
                "######### TransactionType : " + transactionType + "  ======== IPN verified : " + isIpnVerified);
    }
}
