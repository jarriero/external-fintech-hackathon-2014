package com.erudine.hackathon;

import urn.ebay.apis.eBLBaseComponents.PaymentTransactionSearchResultType;

/**
 * Created by Benjamin Woolford-Lim on 21/03/2014.
 */
public class TransactionImpl implements Transaction {

    private String payeeDetails;
    private PaymentTransactionSearchResultType transactionDetails;

    public TransactionImpl(PaymentTransactionSearchResultType transactionDetails, String payeeDetails){
        //TODO Sanity checking/defensive copying.
        this.transactionDetails = transactionDetails;
        this.payeeDetails = payeeDetails;
    }

    @Override
    public String getPayeeDetails() {
        return payeeDetails;
    }

    @Override
    public String getPayerDetails() {
        return transactionDetails.getPayer();
    }

    @Override
    public int getAmountPaidInPence() {
        //TODO Sanity checking.
        Double amount = Double.parseDouble(transactionDetails.getNetAmount().getValue());
        return (int)(amount * 100);
    }

    @Override
    public String getTimestamp() {
        return transactionDetails.getTimestamp();
    }

    @Override
    public String getTransactionID() {
        return transactionDetails.getTransactionID();
    }
}
