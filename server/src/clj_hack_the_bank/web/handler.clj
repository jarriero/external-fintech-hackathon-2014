(ns clj-hack-the-bank.web.handler
  (:use [clojure.tools.logging :only (info debug error)])
  (:require
   [clj-hack-the-bank.allocation :as allocation]
   [compojure.core :only [routes]]
   [clojure.data.json :as json]
   [compojure.response]
   [compojure.handler :as handler]
   [compojure.route :as route]
   [ring.middleware.multipart-params :as mp]
   [ring.middleware.json :as ring-json]
   [ring.util.response :as response]
   [ring.util.codec :as codec :only [url-encode url-decode]]
   [clojure.walk :as walk]
   [net.cgrand.enlive-html :as html :only [replace-vars deftemplate content]]))

(comment
  (html/deftemplate modelPage-template "modelPage.html" [knowledgeModelName start-node var-map situationParameter ]
    [:h2 :a] (html/content knowledgeModelName)
    [:#startNode] (html/content start-node)
    [:#situation] (html/content situationParameter) 
    [:#baseUrl] (html/replace-vars var-map)))

(defn save-user-plan [id new-data datastore]
  (do (swap! datastore assoc-in [id "managed"] new-data)
      ;; should write as well
      ))

(defn create-routes [datastore-store]
  (compojure.core/routes
   (compojure.core/ANY "/userplan/:id" [id] {:headers { "Access-Control-Allow-Origin" "*"}
                                             :body (-> datastore-store :data-store deref (get id) str)})
   (compojure.core/ANY "/saveplan" params (fn [req] (let [{:keys [id data]} (:params req)]
                                                      (save-user-plan id (json/read-str data) (-> datastore-store :data-store)))))
   (compojure.core/ANY "/users" [] (-> datastore-store :data-store deref keys))
   (compojure.core/ANY "/payment-in" params (allocation/allocate-payment
                                             (-> datastore-store :data-store)
                                             (-> datastore-store :paypal-store)                                           
                                             (:params params)))
   (route/resources "/")
   (route/not-found "Not Found")))

(defn wrap-dir-index [handler]
  (fn [req]
    (handler
     (update-in req [:uri]
                #(if (= "/" %) "/index.html" %)))))

(defn wrap-request-logging [handler]
  (fn [{:keys [request-method uri params] :as req}]
    (debug "request coming in"  req )
    (try (let [resp (handler req)]
           (debug "request ended" resp  )
           resp)
         (catch Exception e (debug e)))
    ))

(defn create-handler [datastore-store]
  (->
   (handler/site (create-routes datastore-store))
   wrap-dir-index
   wrap-request-logging
   ring-json/wrap-json-response))
