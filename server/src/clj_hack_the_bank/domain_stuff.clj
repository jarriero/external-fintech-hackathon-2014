(ns clj-hack-the-bank.domain-stuff)

(defn create-datastore-atom [file-name]
  (atom (read-string (slurp file-name))))

(defn create-paypalstore-atom [file-name]
  (atom (read-string (slurp file-name))))
