(ns clj-hack-the-bank.pay-pal
  (require [clj-http.client :as client]))

(def svcs-host "https://svcs.sandbox.paypal.com/")

(defn make-payment [amount from to]
  (let [xxx (str svcs-host "AdaptivePayments/Pay")]
    (client/get
     {:headers {
                "X-PAYPAL-SECURITY-USERID" nil
                "X-PAYPAL-SECURITY-PASSWORD" nil
                "X-PAYPAL-SECURITY-SIGNATURE" nil
                "X-PAYPAL-APPLICATION-ID" "APP-80W284485P519543T"
                "X-PAYPAL-REQUEST-DATA-FORMAT" "JSON"
                "X-PAYPAL-RESPONSE-DATA-FORMAT" "JSON"}
      })))
