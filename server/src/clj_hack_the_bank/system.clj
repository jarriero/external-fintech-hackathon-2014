(ns clj-hack-the-bank.system
  (:require [clj-hack-the-bank.web.webserver :as web-server]
            [clj-hack-the-bank.domain-stuff :as domain-stuff]
            [clj-hack-the-bank.web.handler :as web-handler]
            [com.stuartsierra.component :as component])
  (:gen-class))

(defrecord DataStore [datastore-file-name paypalstore-file-name]
  component/Lifecycle
  (start [this] (assoc this :data-store (domain-stuff/create-datastore-atom datastore-file-name)
                       :paypal-store (domain-stuff/create-paypalstore-atom paypalstore-file-name)))
  (stop [this] (assoc this :data-store nil)))

(defrecord Handler [handler datastore]
  component/Lifecycle
  (start [this] (assoc this :handler (web-handler/create-handler datastore))) 
  (stop [this] (assoc this :handler nil)))

(defrecord WebServer [handler web-server port]
  component/Lifecycle
  (start [this]
    (assoc this :web-server (web-server/create (-> handler :handler) :port port)))
  (stop [this]
    (web-server/stop web-server)
    (assoc this :web-server nil)))

;;;; ___________________________________________________________________________
;;;; ---- system ---
(def webapp-components [:web-server :handler :datastore])

(defrecord JamJarSystem [handler web-server datastore paypalstore]
  component/Lifecycle
  (start [this]
    (component/start-system this webapp-components))
  (stop [this]
    (component/stop-system this webapp-components)))

(defn create-system [config-options]
  (let [{:keys [port datastore-file-name paypalstore-file-name]} config-options]
    (map->JamJarSystem
     {
      :datastore (component/using (map->DataStore {:datastore-file-name datastore-file-name
                                                   :paypalstore-file-name paypalstore-file-name}) [])
      :handler (component/using (map->Handler {})
                                [:datastore])
      :web-server (component/using (map->WebServer {:port port})
                                   [:handler])})))


(def default-config {:port 3000 :datastore-file-name "jam-jar-datastore.edn"
                     :paypalstore-file-name "paypal-account-data.edn"})

(defn -main [& args]
  (println (first args))
  (let [config (if (empty? args) default-config (read-string (first args)))]
    (println config)
              (-> (create-system config) component/start)))
