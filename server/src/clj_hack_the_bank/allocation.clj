(ns clj-hack-the-bank.allocation
  (:import (com.erudine.hackathon AccountImpl)))


(defn allocate-payment [data-store paypal-store params]
  (let [payer-account (AccountImpl.)
        payee-account (AccountImpl.)
        username      (-> @data-store keys first)
        payeraccmap   (get @paypal-store username)
        plan          (sort-by :bucket (:plan (get @data-store username)))
        payermap      (into {} (map (fn [k v] [(name k) (str v)]) 
                                      (keys payeraccmap) (vals payeraccmap)))]
    (. payer-account setUserAccountDetails username)
    (. payer-account setAccount (java.util.HashMap. payermap))
    (for [payment plan]
      (let [stramnt     (:amount payment)
            amount      (if (number? stramnt) stramnt (read-string stramnt))
            payee       (:payee payment)
            payeeaccmap (get @paypal-store payee)
            payeemap    (into {} (map (fn [k v] [(name k) (str v)]) 
                                      (keys payeeaccmap) (vals payeeaccmap)))]
        (. payee-account setUserAccountDetails payee)
        (. payee-account setAccount (java.util.HashMap. payeemap))
        ;(str payermap "\n\n\n" payeemap)
        (. payer-account makePayment payee-account amount)
    ))))
