(defproject web-app ""

  :descriptionp "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [reagent "0.3.0"]
                 [ring/ring-core "1.2.0"]
                 [compojure "1.1.6"]
                 [hiccup "1.0.5"]
                 [ring-middleware-format "0.3.2"]
                 [jarohen/chord "0.2.2"]
                 [jarohen/nomad "0.6.3"]
                 [jarohen/clidget "0.2.0"]
                 [cljs-ajax "0.2.3"]
                 [jarohen/chord "0.2.2"]

                 [com.datomic/datomic-pro "0.9.4360"]
                 [liberator "0.10.0"]

                 [prismatic/dommy "0.1.2"]

                 [org.clojure/core.async "0.1.267.0-0d7780-alpha"]
                 [org.clojure/clojurescript "0.0-2156"]
                 [org.clojure/tools.reader "0.8.3"]

                 [jarohen/frodo-core "0.2.11"]]

  :plugins [[jarohen/lein-frodo "0.2.11"]
            [lein-cljsbuild "1.0.2"]
            [lein-pdo "0.1.1"]
            [com.keminglabs/cljx "0.3.2"]
            [lein-shell "0.4.0"]]

  :frodo/config-resource "clojurenote-config.edn"

  :source-paths ["src/clojure" "target/generated/clj"]

  :resource-paths ["resources" "target/resources"]

  :cljx {:builds [{:source-paths ["src/cljx"]
                   :output-path "target/generated/clj"
                   :rules :clj}

                  {:source-paths ["src/cljx"]
                   :output-path "target/generated/cljs"
                   :rules :cljs}]}

  :cljsbuild {:builds {:dev
                       {:source-paths ["src/cljs" "target/generated/cljs"]
                        :compiler {:output-to "../server/resources/public/js/web-app.js"
                                   :output-dir "../server/resources/public/js/"
                                   :optimizations :whitespace
                                   :pretty-print true

                                   ;; uncomment for source-maps
                                        ; :source-map "target/resources/js/web-app.js.map"
                                   }}

                       :prod
                       {:source-paths ["src/cljs" "target/generated/cljs"]
                        :compiler {:output-to "target/resources/js/web-app.js"
                                   :optimizations :advanced
                                   :pretty-print false
                                   :externs ["externs/jquery.js"]}}}}

  :aliases {"dev" ["do"
                   ["shell" "mkdir" "-p"
                    "target/generated/clj"
                    "target/generated/cljs"
                    "target/resources"]
                   ["cljx" "once"]
                   ["pdo"
                    ["cljx" "auto"]
                    ["cljsbuild" "auto" "dev"]
                    "frodo"]]
            
            "start" ["do"
                     ["cljx" "once"]
                     ["cljsbuild" "once" "prod"]
                     ["trampoline" "frodo"]]})
