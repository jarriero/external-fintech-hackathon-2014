(ns web-app.handler
  (:require [ring.util.response :refer [response]]
            [compojure.core :refer [routes GET]]
            [compojure.route :refer [resources]]
            [compojure.handler :refer [api]]
            [hiccup.page :refer [html5 include-css include-js]]
            [frodo :refer [repl-connect-js]]
            [web-app.db.datomic :refer [connect-db!]]
            [chord.http-kit :as chord]
            [clojure.core.async :refer [<! >! put! close! go go-loop]]))

(defn page-frame []
  (html5
   [:head
    [:title "Basic Web App!"]
    (include-js "//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js")
    (include-js "//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js")
    (include-js "//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js")
    (include-js "//fb.me/react-0.8.0.js")

    (include-js "/js/web-app.js")]
   [:body
    [:div#content]
    [:div#edit-content]
    [:script (repl-connect-js)]]))

(defn evalute-string [command]
  (-> (try
        {:action :success
         :message (-> command
                      read-string
                      eval
                      (doto (prn "<--- EVALUATED")))}
        (catch Exception e {:action :error
                            :message (.getMessage e)}))
      str))

(defn ws-handler [req]
  (chord/with-channel req ws-ch
    (go-loop []
      (let [{:keys [message]} (<! ws-ch)
            {:keys [action content] :as event} (read-string message)]
        (println
         (case action
           :eval (>! ws-ch (evalute-string content))
           "default"))
        (recur)))))

(defn app-routes []
  (routes
    (GET "/ws" [] ws-handler)
    (GET "/" [] (response (page-frame)))
    (resources "/js" {:root "js"})))

(defn app []
  (-> (routes
        (app-routes))
      api))


