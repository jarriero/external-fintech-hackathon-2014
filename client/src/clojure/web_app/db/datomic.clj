(ns web-app.db.datomic
  (:require [datomic.api :as d]
            [web-app.db.schema :as sch]
            [web-app.config :refer [config]]))

(defn db-url []
  (:datomic-url (config)))

(defn reset-db! []
  (let [url (db-url)]
    (d/delete-database url)
    (d/create-database url)))

(defn connect-db! []
  (let [url (db-url)]
    (d/create-database url)
    (doto (d/connect url)
      (d/transact (sch/schema)))))

