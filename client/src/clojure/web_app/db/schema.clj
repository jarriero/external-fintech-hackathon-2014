(ns web-app.db.schema
  (:require [datomic.api :as d]))

(defn make-attribute [attr]
  (assoc attr
    :db/id (d/tempid :db.part/db)
    :db.install/_attribute :db.part/db))

(defn schema []
  (->> [{:db/ident :note/id
         :db/valueType :db.type/uuid
         :db/unique :db.unique/identity
         :db/cardinality :db.cardinality/one}

        {:db/ident :note/title
         :db/valueType :db.type/string
         :db/cardinality :db.cardinality/one}

        {:db/ident :note/content
         :db/valueType :db.type/string
         :db/cardinality :db.cardinality/one}]
       (map make-attribute)))

