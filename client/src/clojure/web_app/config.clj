(ns web-app.config
  (:require [clojure.java.io :as io]
            [nomad :refer [defconfig]]))

(defconfig config
  (io/resource "clojurenote-config.edn"))

