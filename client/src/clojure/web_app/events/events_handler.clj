(ns web-app.events.events-handler
  (:require [clojure.core.async :refer [<! >! put! close! go go-loop]]))

(defn new-system [db-conn]
  (atom {:db-conn db-conn

         ;; the rest of this is up to you
         :users []}))

(defn handle-new-connection! [client-conn]
  (go-loop []
    (when-let [{:keys [messagepar]} (<! client-conn)]
      (println "Message received:" message)
      (recur))))
