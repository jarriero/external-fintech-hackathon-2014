(ns web-app.cljs.app
  (:require [dommy.core :as d]
            [reagent.core :as reagent :refer [atom]]
            [chord.client :refer [ws-ch]]
            [cljs.core.async :refer [<! put! >! chan timeout]]
            ajax.core/AjaxImpl
            [ajax.core :refer [GET POST]]
            clojure.browser.repl)
  (:require-macros [dommy.macros :refer [node sel1]]
                   [cljs.core.async.macros :refer [go-loop go]]))


(def client-name "Joe Bloggs")

(def income 200)

(def bucket-list (atom  [{:uuid "123" :tag "rent" :payee  "landlord" :amount 10 :priority "1"}
                         {:uuid "456" :tag "childcare" :payee "nursery" :amount 50 :priority "1"}
                         {:uuid "890" :tag "electricity" :payee "edf" :amount 50 :priority "2"}]))

(def bucket-color-map
  {1 "#c0392b"
   2 "#1abc9c"})

(defn render-add-buckets []
  (let [tag (atom "")
        payee (atom "")
        amount (atom "")
        priority (atom "")]
    (fn [props]
      [:div
       {:style {:width "200px"}}
       [:div "Tag"
        [:select {:on-change (fn [e]
                               (reset! tag (-> e .-target .-value)))}
         [:option {:value "rent"}
          "Rent"]
         [:option {:value "Mortgage"}
          "Mortgage"]
         [:option {:value "childcare"}
          "childcare"]
         [:option {:value "electricity"}
          "Electricity"]
         [:option {:value "gas"}
          "Gas"]]]
       
       [:div "Payee: "
        [:input {:type "textarea"
                 :style {:clear "both"
                         :float "right"}
                 :value @payee
                 :on-change (fn [e]
                              (reset! payee (-> e .-target .-value)))}]]
       [:div  "Amount"
        [:input {:type "textarea"
                 :style {:clear "both"
                         :float "right"}
                 :value @amount
                 :on-change (fn [e]
                              (reset! amount (-> e .-target .-value)))}]]
       [:div "Priority:"
        [:input {:type "textarea"
                 :style {:clear "both"
                         :float "right"}
                 :value @priority
                 :on-change (fn [e]
                              (reset! priority (-> e .-target .-value)))}]]
       [:input {:type "submit"
                :value "Add"
                :style {:clear "both"
                        :float "left"}
                :on-click (fn [e]
                            (swap! bucket-list conj {:uuid "123"
                                                     :tag @tag
                                                     :payee @payee
                                                     :amount @amount
                                                     :priority  @priority})
                            #_(.alert js/window (str {:id @id
                                                      :payee @payee
                                                      :amount @amount
                                                      :bucket @bucket})))}]])))

(defn render-buckets []
  [:div {:style {:height "200px"}}
   (for [bucket @bucket-list]
     [:div {:style {:width "200px"
                    :height "200px"
                    :float "left"
                    :border "2px solid #bdc3c7"
                    :margin "5px"}}
      #_[:a {:on-click #(reset! bucket-list (remove #{bucket} @bucket-list))}
         "Remove"]
      [:div {:style {:width "200px"}}
       (str (:amount bucket) " pounds")]
      [:div {:style {:width "200px"
                     :background-color (get bucket-color-map (:bucket bucket))}}
       (:payee bucket)]])])

(defn render []
  [:div {}
   (render-buckets)
   [render-add-buckets]])

(set! (.-onload js/window)
      (fn []
        (reagent/render-component [render]
                                  (.-body js/document))))
